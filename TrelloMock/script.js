var taskItem, oldPositionLeft, oldPositionTop, oldPosition;
var tasksList = [];
var tasksForStorage = [];
var returnObj  = JSON.parse(localStorage.getItem("tasks"));
console.log (returnObj);
if ( returnObj !== null)
    {
for (var i = 0; i < returnObj.length; i++)
    {
       NewTask(returnObj[i].id, returnObj[i].name); 
    }
    }


/// add new task

function UpdateLocalStorage(){
    tasksForStorage = [];
    for (var i = 0; i < tasksList.length; i++)
        {
            tasksForStorage.push( { id:tasksList[i].id, name: tasksList[i].textContent } );
            
        }
    localStorage.clear();
    var serialObj = JSON.stringify(tasksForStorage);
    localStorage.setItem("tasks", serialObj);
}


function NewTask(column, name){ 
    taskItem = document.createElement('div');
    taskItem.classList.add('task-item');
    taskItem.setAttribute('id', column);
    taskItem.textContent = name;
    AddEventToTask();
    tasksList.push(taskItem);
    document.getElementById('list' + column).appendChild(taskItem);
  
   
}
function EnterTaskName(){
    return  prompt('Enter name of new task'); 
}
document.getElementById('addInProgress').addEventListener('click', function(){
    var name = EnterTaskName(); 
    var column = 'InProgress';
    NewTask(column, name);
    UpdateLocalStorage();
} );
document.getElementById('addToDo').addEventListener('click', function(){
    var name = EnterTaskName();
    var column = 'ToDo';
    NewTask(column, name);
    UpdateLocalStorage();
} );
document.getElementById('addDone').addEventListener('click', function(){
    var name = EnterTaskName();
    var column = 'Done';
    NewTask(column, name);
    UpdateLocalStorage();
} );


//// drag and drop
var isNeedMove = false;
var newLocation;


function AddEventToTask(){
    taskItem.addEventListener('mousedown', function(e) { /// mousedown
        taskItem  = document.elementFromPoint(e.clientX, e.clientY);
        isNeedMove = true; 
        oldPositionLeft = taskItem.style.left;
        oldPositionTop = taskItem.style.top;
        oldPosition = taskItem.style.position;
        taskItem.style.position = 'fixed';
    }, false);
    taskItem.addEventListener('mouseup', function(e) {  ///  mouseup
        var taskName = taskItem.textContent;
        isNeedMove = false;
        taskItem.hidden = true;
        newLocation = document.elementFromPoint(e.clientX, e.clientY);
        insertIntoNewLocation(taskName);
    });
}

function insertIntoNewLocation(taskName) {
    var selectedTask = taskItem;
     if ( (newLocation.id).includes('ToDo') )
    {
      NewTask('ToDo', taskName);
    }
    else if ( (newLocation.id).includes('Done') )
    {
      NewTask('Done', taskName);
    }
    else if ( (newLocation.id).includes('InProgress') )
    {
        NewTask('InProgress', taskName);
       
        
    }
    else if (  (newLocation.id).includes('delete') )
    {
         selectedTask.parentNode.removeChild(selectedTask);
        tasksList.remove(selectedTask);
         UpdateLocalStorage();
        return;
    }
    else {
        NewTask(taskItem.id,taskName)
    }
      tasksList.remove(selectedTask);
      selectedTask.parentNode.removeChild(selectedTask);  
    UpdateLocalStorage();
}

document.addEventListener('mousemove', function(e){ /// mouse move
    if (isNeedMove)
        {
          moveAt(e.pageX, e.pageY);
        }
});
function moveAt(pageX, pageY) {
    taskItem.style.left = pageX - taskItem.offsetWidth / 2 + 'px';
    taskItem.style.top = pageY - taskItem.offsetHeight / 2 + 'px';
  }

Array.prototype.remove = function(value) {
    var idx = this.indexOf(value);
    if (idx != -1) {
        return this.splice(idx, 1);
    }
    return false;
}